package ru.javacourse.extend;

import java.awt.Graphics;

public class RectComponent extends ShapeComponent{

    @Override
    protected void paintComponent(Graphics g) {
        g.drawRect(getGap(),getGap(),getWidth()-2*getGap(),getHeight()-2*getGap());
    }
}
