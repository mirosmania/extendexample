package ru.javacourse.extend;

import javax.swing.JComponent;
import java.awt.Graphics;

public class OvalComponent extends ShapeComponent{


    @Override
    protected void paintComponent(Graphics g) {
        g.drawOval(getGap(),getGap(),getWidth()-2*getGap(),getHeight()-2*getGap());
    }
}
