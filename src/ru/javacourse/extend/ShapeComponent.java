package ru.javacourse.extend;


import javax.swing.JComponent;


public class ShapeComponent extends JComponent{
    private int gap=0;

    protected int getGap() {
        return gap;
    }

    public void setGap(int gap) {
        this.gap = gap;
    }

    @Override
    public int getWidth(){
        return  super.getWidth()-2*gap;
    }

    @Override
    public int getHeight(){
        return  super.getHeight()-2*gap;
    }


}
