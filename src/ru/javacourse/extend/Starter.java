package ru.javacourse.extend;

import javax.swing.JFrame;

public class Starter {
    public static void main(String[] args) {
        MyFrame frame=new MyFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//закрывая форму,-закрыть приложение
        frame.setBounds(200,200,800,500);//размер формы
        frame.setVisible(true);//покажись
        frame.setTitle("Это моя мега форма");
        frame.initOvals();
    }
}
