package ru.javacourse.extend;

import javax.swing.JFrame;
import java.awt.GridLayout;

public class MyFrame extends JFrame {
    private  ShapeComponent sc;

    public void initOvals(){
        GridLayout gl=new GridLayout(2,3);
        setLayout(gl);

        for (int i=0;i<6;i++){
            //тестирование рисования треугольника
            sc=new TriangleComponent();
            sc.setGap(10);
            add(sc);

            /* тестирование для рисования овала
            sc=new OvalComponent();
            sc.setGap(10);
            add(sc);*/

            /*тестирование рисования прямоуголиника
            sc=new RectComponent();
            sc.setGap(10);
            add(sc);*/
        }
    }

    @Override
    public void setTitle(String title) {
        super.setTitle("Привет,я Ренат-" + title);
    }
}
