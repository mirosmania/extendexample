package ru.javacourse.extend;

import java.awt.Graphics;
import java.awt.Color;
import java.awt.Point;

public class TriangleComponent extends ShapeComponent {


    @Override
    protected void paintComponent(Graphics g) {
        drawTriangle(g);
    }

    private void drawTriangle(Graphics g) {

        g.setColor(Color.BLACK);

        Point location=new Point(getGap(),(getHeight()/2)+getGap());
        Point point2 = new Point(location.x+getWidth(),location.y);
        Point point3 = new Point(location.x+(getWidth()/2),getGap());

        g.drawLine(location.x,location.y,point2.x,point2.y);
        g.drawLine(location.x,location.y,point3.x,point3.y);
        g.drawLine(getWidth(),getHeight()/2+10,point3.x,point3.y);
    }
}
